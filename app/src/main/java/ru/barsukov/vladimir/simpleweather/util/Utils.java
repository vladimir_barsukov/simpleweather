package ru.barsukov.vladimir.simpleweather.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import ru.barsukov.vladimir.simpleweather.R;

/**
 * Util class contains methods to work with shared prefs
 */
public class Utils {

    public static String getCurrentCity(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        return preferences.getString(Constants.PREF_SELECTED_CITY, context.getString(R.string.default_city));
    }

    public static void saveCurrentCity(Context context, String city) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.PREF_SELECTED_CITY, city);
        editor.apply();
    }

    public static long getLastUpdateTime(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        return preferences.getLong(Constants.PREF_LAST_UPDATE, 0);
    }

    public static void saveLastUpdateTime(Context context, long lastUpdateTime) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(Constants.PREF_LAST_UPDATE, lastUpdateTime);
        editor.apply();
    }

    public static String getDayNightTemperature(double temperature) {
        DecimalFormat decimalFormat = new DecimalFormat("#");
        decimalFormat.setRoundingMode(RoundingMode.HALF_DOWN);

        return decimalFormat.format(temperature) + Constants.DEGREE_SYMBOL;
    }

    public static String getCityNameFromLocation(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context, Locale.US);
        List<Address> addresses = null;
        String city = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            Toast.makeText(context, "Error getting addres by location", Toast.LENGTH_SHORT).show();
        }

        if (addresses == null || addresses.isEmpty()) {
            Toast.makeText(context, "No address found", Toast.LENGTH_SHORT).show();
        } else {
            Address address = addresses.get(0);
            city = address.getLocality();
        }

        return city;
    }

    public static int getWeatherIcon(Context context, String iconName) {
        int iconId = R.mipmap.ic_launcher;

        Log.d(Constants.LOG_TAG, "Weather iconName is " + iconName);
        if (iconName == null || iconName.isEmpty()) {
            return iconId;
        }

        iconId = context.getResources().getIdentifier("ic_" + iconName, "mipmap", context.getPackageName());
        return iconId;
    }

}
