package ru.barsukov.vladimir.simpleweather.util;

public class Constants {

    public static final String LOG_TAG = "WeatherLog";

    // URLs and request parameters
    public static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/forecast/";
    public static final String APPID = "a56d23c066c4653f13fdeed788880278";
    public static final String WEATHER_UNITS = "metric";
    public static final String WEATHER_DAYS_COUNT = "7";
    public static final String WEATHER_LANG = "ru";

    // Shared prefs
    public static final String PREFS_NAME = "SimpleWeatherPrefs";
    public static final String PREF_SELECTED_CITY = "SELECTED_CITY";
    public static final String PREF_LAST_UPDATE = "LAST_UPDATE";

    // Database field names
    public static final String COLUMN_NAME_CITYID = "cityId";
    public static final String COLUMN_NAME_WEATHER_DATE = "weatherDate";

    // Intent filter action and extra
    public static final String UPDATE_WEATHER_DATA = "UPDATE_WEATHER_DATA";

    public static final int PERMISSION_REQUEST_CODE = 201;

    public static final String DEGREE_SYMBOL = " \u2103";
    public static final String WEATHER_DATE_FORMAT = "dd.MM";
    public static final String WEATHER_TIME_FORMAT = "HH:mm";

}
