package ru.barsukov.vladimir.simpleweather.application;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import ru.barsukov.vladimir.simpleweather.database.HelperFactory;

public class MyApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        // TODO uncommit later
        // Fabric.with(this, new Crashlytics());
        MultiDex.install(this);
        HelperFactory.setHelper(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
