package ru.barsukov.vladimir.simpleweather.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import ru.barsukov.vladimir.simpleweather.model.WeatherModel;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "weatherdb.db";
    private static final int DATABASE_VERSION = 1;

    private WeatherDAO mWeatherDAO;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, WeatherModel.class);
        } catch (SQLException e) {
            Log.e("", "error creating DB " + DATABASE_NAME);
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, WeatherModel.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public WeatherDAO getWeatherDAO() throws SQLException {
        if (mWeatherDAO == null)
            mWeatherDAO = new WeatherDAO(getConnectionSource(), WeatherModel.class);

        return mWeatherDAO;
    }

    /**
     * Close any open connections.
     */
    @Override
    public void close() {
        super.close();
        mWeatherDAO = null;
    }
}
