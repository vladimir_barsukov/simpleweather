package ru.barsukov.vladimir.simpleweather.util;

import android.util.Log;

import java.sql.SQLException;
import java.util.List;

import ru.barsukov.vladimir.simpleweather.database.DatabaseHelper;
import ru.barsukov.vladimir.simpleweather.database.HelperFactory;
import ru.barsukov.vladimir.simpleweather.database.WeatherDAO;
import ru.barsukov.vladimir.simpleweather.model.WeatherModel;
import ru.barsukov.vladimir.simpleweather.pojo.City;
import ru.barsukov.vladimir.simpleweather.pojo.Temp;
import ru.barsukov.vladimir.simpleweather.pojo.Weather;
import ru.barsukov.vladimir.simpleweather.pojo.WeatherPOJO;

public class DatabaseUtils {

    public static void saveDataToDataBase(WeatherPOJO pojo) {
        DatabaseHelper helper = HelperFactory.getHelper();
        try {
            // Trying to find old data in database and remove all
            WeatherDAO weatherDao = helper.getWeatherDAO();
            weatherDao.deleteAll();

            // Trying to save new data to database
            for (ru.barsukov.vladimir.simpleweather.pojo.List list : pojo.getList()) {
                WeatherModel model = new WeatherModel();

                City city = pojo.getCity();
                if (city != null && city.getCoord() != null) {
                    model.setCityId(city.getId());
                    model.setLatitude(city.getCoord().getLat());
                    model.setLongitude(city.getCoord().getLon());
                }
                List<Weather> weathers = list.getWeather();
                if (weathers != null && !weathers.isEmpty()) {
                    Weather weather = weathers.get(0);
                    model.setWeatherMain(weather.getMain());
                    model.setWeatherDescription(weather.getDescription());
                    model.setIcon(weather.getIcon());
                }
                Temp temp = list.getTemp();
                model.setTempDay(temp.getDay());
                model.setTempMax(temp.getMax());
                model.setTempMin(temp.getMin());
                model.setTempNight(temp.getNight());
                model.setWeatherDate(list.getDt());

                weatherDao.create(model);
            }
        } catch (SQLException e) {
            Log.e(Constants.LOG_TAG, "Error creating weather model object. SQL State: " + e.getSQLState() + "\nMessage: " + e.getMessage() + "\nCause: " + e.getCause());
        }
    }

    public static List<WeatherModel> getWeatherFromDatabase() {
        List<WeatherModel> weatherModelList;
        DatabaseHelper helper = HelperFactory.getHelper();
        try {
            WeatherDAO weatherDao = helper.getWeatherDAO();
            weatherModelList = weatherDao.getAllWeather();
        } catch (SQLException e) {
            throw new RuntimeException("Error getting records " + e);
        }

        return weatherModelList;
    }

}
