package ru.barsukov.vladimir.simpleweather.pojo;

public class Temp {

    private double min;

    private double eve;

    private double max;

    private double morn;

    private double night;

    private double day;

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getEve() {
        return eve;
    }

    public void setEve(double eve) {
        this.eve = eve;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getMorn() {
        return morn;
    }

    public void setMorn(double morn) {
        this.morn = morn;
    }

    public double getNight() {
        return night;
    }

    public void setNight(double night) {
        this.night = night;
    }

    public double getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "Temp [min = " + min + ", eve = " + eve + ", max = " + max + ", morn = " + morn + ", night = " + night + ", day = " + day + "]";
    }

}
