package ru.barsukov.vladimir.simpleweather.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import ru.barsukov.vladimir.simpleweather.model.WeatherModel;
import ru.barsukov.vladimir.simpleweather.util.Constants;

public class WeatherDAO extends BaseDaoImpl<WeatherModel, Integer> {
    /**
     * Construct our base DAO class. The dataClass provided must have its fields marked with {@link DatabaseField} or
     * javax.persistance annotations.
     *
     * @param connectionSource Source of our database connections.
     * @param dataClass
     */
    protected WeatherDAO(ConnectionSource connectionSource, Class<WeatherModel> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<WeatherModel> getAllWeather() throws SQLException {
        DatabaseHelper helper = HelperFactory.getHelper();

        WeatherDAO weatherDAO = helper.getWeatherDAO();
        QueryBuilder<WeatherModel, Integer> builder = weatherDAO.queryBuilder();
        builder.orderBy(Constants.COLUMN_NAME_WEATHER_DATE, true);
        PreparedQuery<WeatherModel> preparedQuery = builder.prepare();
        List<WeatherModel> weatherList = weatherDAO.query(preparedQuery);

        return weatherList;
    }

    public void deleteAll() throws SQLException {
        DatabaseHelper helper = HelperFactory.getHelper();

        WeatherDAO weatherDAO = helper.getWeatherDAO();
        List<WeatherModel> modelList = weatherDAO.getAllWeather();
        if (modelList != null && !modelList.isEmpty())
            weatherDAO.delete(modelList);
    }
}
