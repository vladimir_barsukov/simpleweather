package ru.barsukov.vladimir.simpleweather.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.barsukov.vladimir.simpleweather.R;
import ru.barsukov.vladimir.simpleweather.model.WeatherModel;
import ru.barsukov.vladimir.simpleweather.util.Constants;
import ru.barsukov.vladimir.simpleweather.util.Utils;

/**
 * RecyclerView adapter
 */
public class RvAdapter extends RecyclerView.Adapter<RvAdapter.ViewHolder> {

    private Context mContext;
    private List<WeatherModel> mList;

    public RvAdapter(Context context, List<WeatherModel> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        WeatherModel model = mList.get(position);
        String weatherText = model.getWeatherDescription();

        SimpleDateFormat sdf = new SimpleDateFormat(Constants.WEATHER_DATE_FORMAT);
        String date = sdf.format(model.getWeatherDate() * 1000);
        int icon = Utils.getWeatherIcon(mContext, model.getIcon());
        String dayTemp = Utils.getDayNightTemperature(model.getTempDay());
        String nightTemp = Utils.getDayNightTemperature(model.getTempNight());

        holder.mWeatherText.setText(weatherText);
        holder.mWeatherDescription.setText(date);
        holder.mWeatherIcon.setImageResource(icon);
        holder.mWeatherTempDay.setText(dayTemp);
        holder.mWeatherTempNight.setText(nightTemp);
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return (mList != null ? mList.size() : 0);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cv_weather_item) CardView mCvWeatherItem;
        @BindView(R.id.iv_weather_icon) ImageView mWeatherIcon;
        @BindView(R.id.tv_weather_text) TextView mWeatherText;
        @BindView(R.id.weather_description) TextView mWeatherDescription;
        @BindView(R.id.tv_weather_temp_day) TextView mWeatherTempDay;
        @BindView(R.id.tv_weather_temp_night) TextView mWeatherTempNight;

        public ViewHolder(View rootView) {
            super(rootView);
            ButterKnife.bind(this, rootView);
        }
    }

}
