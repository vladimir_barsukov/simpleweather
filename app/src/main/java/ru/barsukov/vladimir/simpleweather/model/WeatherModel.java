package ru.barsukov.vladimir.simpleweather.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import ru.barsukov.vladimir.simpleweather.util.Constants;

/**
 * Weather model class
 */
@DatabaseTable(tableName = "weather")
public class WeatherModel {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = Constants.COLUMN_NAME_CITYID)
    private String cityId;

    @DatabaseField
    private double latitude;

    @DatabaseField
    private double longitude;

    @DatabaseField
    private double tempDay;

    @DatabaseField
    private double tempMin;

    @DatabaseField
    private double tempMax;

    @DatabaseField
    private double tempNight;

    @DatabaseField
    private String weatherMain;

    @DatabaseField
    private String weatherDescription;

    @DatabaseField
    private String icon;

    @DatabaseField(columnName = Constants.COLUMN_NAME_WEATHER_DATE)
    private long weatherDate;

    public WeatherModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getTempDay() {
        return tempDay;
    }

    public void setTempDay(double tempDay) {
        this.tempDay = tempDay;
    }

    public double getTempMin() {
        return tempMin;
    }

    public void setTempMin(double tempMin) {
        this.tempMin = tempMin;
    }

    public double getTempMax() {
        return tempMax;
    }

    public void setTempMax(double tempMax) {
        this.tempMax = tempMax;
    }

    public double getTempNight() {
        return tempNight;
    }

    public void setTempNight(double tempNight) {
        this.tempNight = tempNight;
    }

    public String getWeatherMain() {
        return weatherMain;
    }

    public void setWeatherMain(String weatherMain) {
        this.weatherMain = weatherMain;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public long getWeatherDate() {
        return weatherDate;
    }

    public void setWeatherDate(long weatherDate) {
        this.weatherDate = weatherDate;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
