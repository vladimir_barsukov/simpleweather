package ru.barsukov.vladimir.simpleweather;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.barsukov.vladimir.simpleweather.adapter.RvAdapter;
import ru.barsukov.vladimir.simpleweather.model.WeatherModel;
import ru.barsukov.vladimir.simpleweather.util.Constants;
import ru.barsukov.vladimir.simpleweather.util.DatabaseUtils;
import ru.barsukov.vladimir.simpleweather.util.Utils;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private UIUpdaterBroadcastReceiver mReceiver;
    private GoogleApiClient mGoogleApiClient;

    @BindView(R.id.iv_today_weather_icon) ImageView mIvWeatherIcon;
    @BindView(R.id.tv_city_name) TextView mTvCityName;
    @BindView(R.id.tv_temperature_day) TextView mTvTemperatureDay;
    @BindView(R.id.tv_temperature_night) TextView mTvTemperatureNight;
    @BindView(R.id.tv_weather_description) TextView mTvWeatherDescription;
    @BindView(R.id.tv_weather_updated) TextView mTvWeatherUpdated;
    @BindView(R.id.refresh_layout) SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.rv_weather_list) RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        registerReceiver();
        ButterKnife.bind(this);

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.primary_dark, R.color.accent);

        showLoadedWeather();

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestMultiplePermissions();
            }

            updateWeather();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mReceiver != null)
            unregisterReceiver(mReceiver);
    }

    @Override
    protected void onStart() {
        if (mGoogleApiClient != null) mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null) mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.weather_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_sync:
                Toast.makeText(this, getString(R.string.toast_update_weather), Toast.LENGTH_SHORT).show();
                updateWeather();
                return true;
            case R.id.action_kazan:
                Utils.saveCurrentCity(this, getString(R.string.default_city));
                updateWeather();
                return true;
            case R.id.action_london:
                Utils.saveCurrentCity(this, getString(R.string.london_city));
                updateWeather();
                return true;
            case R.id.action_moscow:
                Utils.saveCurrentCity(this, getString(R.string.moscow_city));
                updateWeather();
                return true;
            case R.id.action_vladivostok:
                Utils.saveCurrentCity(this, getString(R.string.vladivostok_city));
                updateWeather();
                return true;
            case R.id.action_current_position:
                getLocation();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Called when a swipe gesture triggers a refresh.
     */
    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        updateWeather();
    }

    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.UPDATE_WEATHER_DATA);
        mReceiver = new UIUpdaterBroadcastReceiver();
        registerReceiver(mReceiver, intentFilter);
    }

    public void requestMultiplePermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{
                        android.Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                },
                Constants.PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Constants.PERMISSION_REQUEST_CODE && grantResults.length == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getLocation();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Получает текущие координаты
     */
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestMultiplePermissions();
        } else {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (lastLocation != null) {
                String city = Utils.getCityNameFromLocation(this, lastLocation.getLatitude(), lastLocation.getLongitude());
                if (city != null)
                    Utils.saveCurrentCity(this, city);
                updateWeather();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    class UIUpdaterBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            showLoadedWeather();
        }
    }

    private void updateWeather() {
        Intent intent = new Intent(this, WeatherLoaderService.class);
        startService(intent);
    }

    private void showLoadedWeather() {
        mSwipeRefreshLayout.setRefreshing(false);
        List<WeatherModel> weatherList = DatabaseUtils.getWeatherFromDatabase();

        // If database is empty then nothing to update
        if (weatherList == null || weatherList.isEmpty()) return;

        WeatherModel todayWeather = weatherList.get(0);

        int icon = Utils.getWeatherIcon(this, todayWeather.getIcon());
        String dayTemp = Utils.getDayNightTemperature(todayWeather.getTempDay());
        String nightTemp = Utils.getDayNightTemperature(todayWeather.getTempNight());
        String selectedCity = Utils.getCurrentCity(this);
        if (selectedCity == null)
            selectedCity = getString(R.string.default_city);

        mIvWeatherIcon.setImageResource(icon);
        mTvCityName.setText(selectedCity);
        mTvTemperatureDay.setText(dayTemp);
        mTvTemperatureNight.setText(nightTemp);
        mTvWeatherDescription.setText(todayWeather.getWeatherDescription());

        long updateTime = Utils.getLastUpdateTime(this);
        if (updateTime == 0) {
            mTvWeatherUpdated.setVisibility(View.GONE);
            Log.d(Constants.LOG_TAG, "Update time is  " + updateTime);
        } else {
            // SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.WEATHER_TIME_FORMAT);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.WEATHER_TIME_FORMAT, Locale.getDefault());
            String time = simpleDateFormat.format(updateTime);
            mTvWeatherUpdated.setVisibility(View.VISIBLE);
            mTvWeatherUpdated.setText(getString(R.string.text_updated) + " " + time);
            Log.d(Constants.LOG_TAG, "Updated at " + time);
        }

        weatherList.remove(0);

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        RvAdapter adapter = new RvAdapter(this, weatherList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setItemAnimator(itemAnimator);
    }

}
