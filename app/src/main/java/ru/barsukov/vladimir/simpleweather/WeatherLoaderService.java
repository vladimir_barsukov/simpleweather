package ru.barsukov.vladimir.simpleweather;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.barsukov.vladimir.simpleweather.network.ApiService;
import ru.barsukov.vladimir.simpleweather.pojo.WeatherPOJO;
import ru.barsukov.vladimir.simpleweather.util.Constants;
import ru.barsukov.vladimir.simpleweather.util.DatabaseUtils;
import ru.barsukov.vladimir.simpleweather.util.Utils;

public class WeatherLoaderService extends IntentService {

    private WeatherPOJO mWeatherPojo;

    public WeatherLoaderService() {
        super(WeatherLoaderService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        getWeather();
    }

    private void getWeather() {
        String city = Utils.getCurrentCity(this);
        ApiService apiService = ApiService.retrofit.create(ApiService.class);
        Call<WeatherPOJO> call = apiService.getWeatherByCityName(city, Constants.APPID, Constants.WEATHER_UNITS, Constants.WEATHER_DAYS_COUNT);
        call.enqueue(new Callback<WeatherPOJO>() {
            @Override
            public void onResponse(Call<WeatherPOJO> call, Response<WeatherPOJO> response) {
                mWeatherPojo = response.body();
                if (mWeatherPojo != null) {
                    // save data to database
                    DatabaseUtils.saveDataToDataBase(mWeatherPojo);
                    Utils.saveLastUpdateTime(WeatherLoaderService.this, new Date().getTime());

                    // TODO remove
                    Log.d(Constants.LOG_TAG, "Saved last update time " + Utils.getLastUpdateTime(WeatherLoaderService.this));

                    // notify activity to update UI
                    Intent intent = new Intent(Constants.UPDATE_WEATHER_DATA);
                    WeatherLoaderService.this.sendBroadcast(intent);
                }
            }

            @Override
            public void onFailure(Call<WeatherPOJO> call, Throwable t) {
                Log.e(Constants.LOG_TAG, "Failed to get weather: " + t.getMessage());
            }
        });
    }
}
