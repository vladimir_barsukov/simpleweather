package ru.barsukov.vladimir.simpleweather.network;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.barsukov.vladimir.simpleweather.pojo.WeatherPOJO;
import ru.barsukov.vladimir.simpleweather.util.Constants;

public interface ApiService {

    @GET("daily")
    Call<WeatherPOJO> getWeatherByCityName(@Query("q") String cityName, @Query("APPID") String appId, @Query("units") String units, @Query("cnt") String numberOfDays);

    public Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.WEATHER_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
