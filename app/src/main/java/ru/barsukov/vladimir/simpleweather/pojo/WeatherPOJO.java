package ru.barsukov.vladimir.simpleweather.pojo;

/**
 * Main weather POJO class that used in json parsing
 */
public class WeatherPOJO {

    private City city;

    private String cod;

    private String message;

    private String cnt;

    private java.util.List<List> list;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "WeatherPojo [city = " + city + ", cod = " + cod + ", message = " + message + ", cnt = " + cnt + ", list = " + list + "]";
    }

}
